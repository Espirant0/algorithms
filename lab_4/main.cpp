#include <iostream>
#include <chrono>

#define N 15

void CombSort(int n, int mas[])
{
	double factor = 1.2473309; 
	int k = n - 1; 
	while (k >= 1){
		for (int i = 0; i + k < n; i++){
			if (mas[i] > mas[i + k]){
				std::swap(mas[i], mas[i + k]);
			}
		}
		k /= factor;
	}
};


int main() {
	srand(time(0));
	int mas[N];
	for (int i = 0; i < N; i++)
		mas[i] = rand() % 100;
	for (int i = 0; i < N; i++)
		std::cout << mas[i] << std::endl;
	std::cout << "=================" << std::endl;
    CombSort(N, mas);
    for (int i = 0; i < N; i++)
        std::cout << mas[i] << std::endl;
	return 0;
}

//worst - n^2
//medium - n^2
//best - nlogn