mas = [12, 5, 664, 63, 5, 73, 93, 127, 432, 64, 34]
print(mas)
length = len(str(max(mas))) #максимальное количество разрядов
rang = 10 #число корзин (десятичная система - 10 цифр)
for i in range(length):
    bracket = [[] for k in range(rang)] #список длины range, состоящий из пустых списков
    for x in mas:
        figure = x // 10**i % 10  #раскладывает числа по корзинам, сортируя по каждому разряду
        bracket[figure].append(x)
    mas = []
    for k in range(rang):     #собираем все отсортированные корзины в один массив
        mas = mas + bracket[k]
print(mas)