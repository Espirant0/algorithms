#include <iostream>
#include <chrono>

#define N 15

void ShellSort(int n, int mas[])
{
    int i, j;
    for (int k = n / 2; k > 0; k /= 2)
        for (i = k; i < n; i++)
        {
            for (j = i; j >= k; j -= k)
            {
                if (mas[j] < mas[j - k])
                    std::swap(mas[j], mas[j - k]);
                else
                    break;
            }
        }
};


int main() {
	srand(time(0));
	int mas[N];
	for (int i = 0; i < N; i++)
		mas[i] = rand() % 100;
	for (int i = 0; i < N; i++)
		std::cout << mas[i] << std::endl;
	std::cout << "=================" << std::endl;
    ShellSort(N, mas);
    for (int i = 0; i < N; i++)
        std::cout << mas[i] << std::endl;
	return 0;
}

//O(nlog^2n) - best
//O(n^2) - worst