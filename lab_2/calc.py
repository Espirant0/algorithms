import sys
print("Введите выражение:")
def CheckBrackets(s): # Проверка на правильность скобок
    mas = []
    flag = False
    for i in range(len(s)):
        if s[i] == '(':
            mas.append(i)
            flag = True
        if s[i] == ')': # При закрытии скобок если массив не пуст, то удаляем открытую скобку, иначе выражение неверно
            if mas:
                mas.pop()
            else:
                print("Неправвильно расставлены скобки!")
                return 0
    if (len(mas) == 0 and flag):
        return 2
    if (len(mas) == 0 and not flag): # Функция не заходила в if'ы, т.е. в выражении нет скобок
        return 1
    else:
        print("Неправвильно расставлены скобки!")
        return 0

def Brackets(mas): # Поиск скобок в выражении и вывод индексов выражения в скобках
    start = 0
    end = 0
    i = 0
    while mas[i]!=')':
        if mas[i] == '(':
            start = i + 1
        i += 1
    end = i - 1
    return start, end

def Count(mas, first, second): # Функция вычисления
    result = 0
    if mas[first + 1] == '+':
        result = float(mas[first]) + float(mas[second])
    if mas[first + 1] == '-':
        result = float(mas[first]) - float(mas[second])
    if mas[first + 1] == '*':
        result = float(mas[first]) * float(mas[second])
    if (mas[first + 1] == '/'):
        result = float(mas[first]) / float(mas[second])
    mas[first] = str(result)
    del mas[first+1:second+1] # Удаляем знак и второе число, т.к. результат останется на месте первого числа
    return mas

def Array(s): # Разделяем выражение на элементы из чисел и знаков и добавляем в массив
    mas = []
    i = 0
    while i<len(s):
        if (s[i].isdigit()): #Если текущее значение - число, то ищем конец этого числа, затем добавляем в конец массива полное число
            k = 0
            while s[i+k].isdigit():
                k += 1
            else:
                mas.append(s[i:i+k])
                i = i+k
        else:                 # Если текущий элемент не число, то добавляем его в конец массива
            mas.append(s[i])
            i += 1
    return mas

s = str(input())
mas = Array(s)
x = CheckBrackets(s)

if x:                                  #Если выражение прошло проверку на скобки
    while '(' in mas:                  #Сначала Вычисляем скобки, чтобы сохранить порядок
        start, end = Brackets(mas)     #Индексы начала и конца скобок
        new_end = end                  #Новый индекс конца, чтобы удалять уже вычисленные выражения
        while new_end != start:
            k = 0                      #Коэф., на который следует уменьшать выражение
            if (('*' in mas[start:end]) or ('/' in mas[start:end])):# Сначала вычислим * и /, чтобы сохранить порядок
                for i in range(start, end):
                    if i < len(mas):
                        try:
                            if mas[i] == '*' or mas[i] == '/':# Производим вычисления. Перехватываем деление на ноль, чтобы не было ошибок
                                mas = Count(mas, i-1, i+1)
                                k += 2                        # Прибавляем 2, т.к. нужно избавиться от числа и знака
                        except ZeroDivisionError:
                            print("На ноль делить нельзя!")
                            sys.exit(0)                       # Выход из программы при возникновении ошибки
                    else:
                        break
                new_end = new_end - k
            else:                                             # Теперь вычисляем + и -
                for i in range(start, new_end):
                    if i < len(mas):
                        if mas[i] == '+' or mas[i] == '-':
                            mas = Count(mas, i-1, i+1)
                            k += 2
                    else:
                        break
                new_end = new_end - k
        del mas[start+1]
        del mas[start-1]


    while len(mas) > 2:                                       # >2, т.к. в массиве всегда остается число и знак "="
        k = 0                                                           
        if (('*' in mas) or ('/' in mas)):                    # Сначала вычислим * и /, чтобы сохранить порядок
            for i in range(len(mas)):
                if i < len(mas):
                    try:
                        if mas[i] == '*' or mas[i] == '/':
                            mas = Count(mas, i - 1, i + 1)
                            k += 1
                    except ZeroDivisionError:
                        print("На ноль делить нельзя!")
                        sys.exit(0)
                else:
                    break
        else:                                                 # Теперь вычислим + и -
            for i in range(len(mas)):
                if i < len(mas):
                    if mas[i] == '-' or mas[i] == '+':
                        mas = Count(mas, i - 1, i + 1)
                        k += 1
                else:
                    break
if len(mas) == 2:                                             #Вывод ответа 
    print('Ответ:', mas[0])
