#include <iostream>
#include <chrono>

#define N 15

void SelectionSort(int n, int mas[])
{
    for (int i = 0; i < n - 1; i++)
    {
        int min = i;
        for (int j = i + 1; j < n; j++)
        {
            if (mas[j] < mas[min])
            {
                min = j;
            }
        }
        if (min != i)
        {
            std::swap(mas[i], mas[min]);
        }
    }
};


int main() {
	srand(time(0));
	int mas[N];
	for (int i = 0; i < N; i++)
		mas[i] = rand() % 100;
	for (int i = 0; i < N; i++)
		std::cout << mas[i] << std::endl;
	std::cout << "=================" << std::endl;
    SelectionSort(N, mas);
    for (int i = 0; i < N; i++)
        std::cout << mas[i] << std::endl;
	return 0;
}


//O(n^2)