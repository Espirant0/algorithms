#include <iostream>
#include <chrono>

#define N 15

void Heapify(int mas[], int n, int root)
{
    int largest = root;             //�������������� ���������� ������� ��� ������          
    int left = 2 * root + 1;        //����� 
    int right = 2 * root + 2;       //������ 

    if (left < n && mas[left] > mas[largest]) //���� ����� �������� ������� ������ �����
        largest = left;

    if (right < n && mas[right] > mas[largest])//���� ������ �������� ������� ������, ��� ����� ������� ������� 
        largest = right;

    if (largest != root){                       // ���� ����� ������� ������� �� ������
        std::swap(mas[root], mas[largest]);
        Heapify(mas, n, largest);             //���������� ����������� � �������� ���� ���������� ���������
    }
}

void HeapSort(int mas[], int n)
{
    for (int i = n / 2 - 1; i >= 0; i--)        //���������� ���� (�������������� ������)
        Heapify(mas, n, i);

    for (int i = n - 1; i >= 0; i--){        //���� �� ������ ��������� �������� �� ����
        std::swap(mas[0], mas[i]);          //���������� ������� ������ � �����
        Heapify(mas, i, 0);                 //�������� ��������� heapify �� ����������� ����
    }
}


int main() {
	srand(time(0));
	int mas[N];
	for (int i = 0; i < N; i++)
		mas[i] = rand() % 100;
	for (int i = 0; i < N; i++)
		std::cout << mas[i] << std::endl;
	std::cout << "=================" << std::endl;
    HeapSort(mas, N);
    for (int i = 0; i < N; i++)
        std::cout << mas[i] << std::endl;
	return 0;
}

//O(nlogn)