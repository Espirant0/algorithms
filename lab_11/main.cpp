#include <iostream>
#define N 10

void QuickSort(int mas[N], int a, int b)
{
    if (a >= b)
        return;
    int m = (rand() * rand()) % (b - a + 1) + a;
    int k = mas[m];
    int l = a - 1;
    int r = b + 1;
    while (1)
    {
        do l = l + 1; while (mas[l] < k);
        do r = r - 1; while (mas[r] > k);
        if (l >= r)
            break;
        std::swap(mas[l], mas[r]);
    }
    r = l;
    l = l - 1;
    QuickSort(mas, a, l);
    QuickSort(mas, r, b);
}

int main() {
    srand(time(0));
    int mas[N];
    for (int i = 0; i < N; i++)
        mas[i] = rand();
    for (int i = 0; i < N; i++)
        std::cout << mas[i] << std::endl;
    int n = N;
    int a = 0;
    int b = N - 1;
    QuickSort(mas, a, b);
    std::cout << "================" << std::endl;
    for (int i = 0; i < N; i++)
        std::cout << mas[i] << std::endl;
    
	return 0;
}