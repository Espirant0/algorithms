#include <iostream>
#include <chrono>

#define N 15

void InsertionSort(int n, int mas[])
{
	for (int i = 0; i < N; i++)
		for (int j = i; j > 0 && mas[j - 1] > mas[j]; j--)
			std::swap(mas[j - 1], mas[j]); 
};


int main() {
	srand(time(0));
	int mas[N];
	for (int i = 0; i < N; i++)
		mas[i] = rand() % 100;
	for (int i = 0; i < N; i++)
		std::cout << mas[i] << std::endl;
	std::cout << "=================" << std::endl;
    InsertionSort(N, mas);
    for (int i = 0; i < N; i++)
        std::cout << mas[i] << std::endl;
	return 0;
}

//O(n) - best
//O(n^2) - medium
//O(n^2) - worst
